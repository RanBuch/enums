exports.genders = [{
    "key": "male",
    "value": 1
}, {
        "key": "female",
        "value": 2
    }];
exports.wearableGenders = [{
    "key": "male",
    "value": 1
}, {
        "key": "female",
        "value": 2
    }, {
        "key": "unisex",
        "value": 3
    }],
    exports.wearableTypes = [{
        "key": "dress",
        "value": 1
    }, {
            "key": "shirt",
            "value": 2
        }, {
            "key": "pants",
            "value": 3
        }, {
            "key": "skirt",
            "value": 4
        }, {
            "key": "shorts",
            "value": 5
        }, {
            "key": "coat / jacket",
            "value": 6
        }, {
            "key": "swimwear",
            "value": 7
        }, {
            "key": "sportswear",
            "value": 8
        }, {
            "key": "sleeveless",
            "value": 9
        }, {
            "key": "vest",
            "value": 10
        }],
    exports.wearableFits = [{
        "key": "slim fit",
        "value": 1
    }, {
            "key": "regular fit",
            "value": 2
        }, {
            "key": "tight fit",
            "value": 3
        }],
    exports.wearableSizes = [{
        "key": "xxs",
        "value": 1,
        "description": "extra extra small",
        "avatarSupport": false
    }, {
            "key": "xs",
            "value": 2,
            "description": "extra small",
            "avatarSupport": false
        }, {
            "key": "s",
            "value": 3,
            "description": "small",
            "avatarSupport": true
        }, {
            "key": "m",
            "value": 4,
            "description": "medium",
            "avatarSupport": true
        }, {
            "key": "l",
            "value": 5,
            "description": "large",
            "avatarSupport": true
        }, {
            "key": "xl",
            "value": 6,
            "description": "extra large",
            "avatarSupport": true
        }, {
            "key": "xxl",
            "value": 7,
            "description": "extra extra large",
            "avatarSupport": true
        }],
    exports.wearableSizeRegions = [{
        "key": "us",
        "value": 1
    }, {
            "key": "uk",
            "value": 2
        }, {
            "key": "it",
            "value": 3
        }, {
            "key": "fr",
            "value": 4
        }, {
            "key": "jp",
            "value": 5
        }],
    exports.fabricTypes = [{
        "key": "cotton",
        "value": 1
    }, {
            "key": "polyester",
            "value": 2
        }, {
            "key": "nylon",
            "value": 3
        }, {
            "key": "carpet",
            "value": 4
        }],
    exports.colors = [{
        "key": "uniform",
        "value": 1
    }, {
            "key": "colorful",
            "value": 2
        }, {
            "key": "black",
            "value": 3
        }, {
            "key": "white",
            "value": 4
        }],
    exports.resourceTypes = [{
        "key": "image",
        "description": "image source",
        "value": 1
    }, {
            "key": "wizard",
            "description": "3d model",
            "value": 2
        }, {
            "key": "video",
            "description": "video tag",
            "value": 3
        }, {
            "key": "iframe",
            "description": "any iframe",
            "value": 4
        }]
exports.usersTypes = [{
    "key": "admin",
    "value": 1
}, {
        "key": "retailer",
        "value": 2
    }, {
        "key": "shopper",
        "value": 3
    }];
exports.tourTypes = ["pants", "shirt", "underwear"];
exports.modelCreationMethods = [
    {
        key: 1,
        value: 'model file (zip/rar)',
        description: 'a zip or rar file which contains at least 1 texture image, only one *.mtl file and only one *.obj file'
    },
    {
        key: 2,
        value: '2 images (front & back)',
        description: '2 photos - 1 for the front of the item and 1 for the back. the images should be of a PNG format and the item shots should be made on a green surface'
    },
    {
        key: 3,
        value: '3 images (front, back & preset)',
        description: '3 photos - 1 of the front, 1 of the back and 1 of the front with preset - the item\'s openings are marked and the back parts are removed from the photo'
    }
];
exports.garmentRejectsTypes = [
    {
        key: 1,
        value: 'shape related',
        description: 'something\'s wrong with the shape of the model'
    },
    {
        key: 2,
        value: 'texture related',
        description: 'something\'s wrong with the texture of the model'
    },
    {
        key: 3,
        value: 'other',
        description: 'any other issue'
    }
];

exports.creatorsJobsStatus = [
    {
        key: 1,
        value: 'ARTISTS UI'
    },
    {
        key: 2,
        value: 'APPROVED'
    },
    {
        key: 3,
        value: 'PENDING'
    },
    {
        key: 4,
        value: 'REJECTED'
    }
];

exports.viewerTypes = [
    {
        key: 'v3d',
        value: 'GLTF Viewer'
    },
    {
        key: 'b4w',
        value: 'Blend4Web Viewer'
    }
];

exports.preProcessedStatus = ['Offline', 'Request Created', 'Pending Internal Review', 'In Progress', 'Live', 'Rejected', 'Approved Internally'];

exports.preProcessedPriority = [
    {
        key: 1,
        value: 'Backlog'
    },
    {
        key: 5,
        value: 'ASAP'
    }
];